function onLoad(){    
	const temperature_rect_count = 10;
	
	//returns random rect as THREE.Vector4(lowx, lowy, highx, highy)
	//coordinates ∈ [0, 1]
	function random_rect(){
		var x=Math.random(),y=Math.random();
		var width=Math.random()/5,height=Math.random()/5;
		return new THREE.Vector4( 
			x, y,
			x+width, y+height );
	}
	
	function generate_temperature_rects(uniforms) {
		uniforms.temperatures.value = 
			Array.from({length: temperature_rect_count}, Math.random);
		uniforms.rects.value = 
			Array.from({length: temperature_rect_count}, random_rect);
	}

	function make_render_target(){
		var target = {};
		target.scene = new THREE.Scene();
		target.bufferTexture = new THREE.WebGLRenderTarget( 
			window.innerWidth, 
			window.innerHeight, 
			{ minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter}
		);
		target.camera = new THREE.OrthographicCamera(-1,1,1,-1,0.1, 100);
		target.camera.position.z = 50;
		target.scene.add(target.camera);
		
		target.shaderMaterial = new THREE.ShaderMaterial( {
			vertexShader: vertex_text,
			fragmentShader: fragment_texture_pass_through_text,
			uniforms: {
				TEXTURE_SIZE: { type: "2f", value: [WIDTH, HEIGHT] },
				texture: {type: 't', value: null}
			}
		});
		var geometry = new THREE.BufferGeometry();
		// create a simple square shape. We duplicate the top left and bottom right
		// vertices because each vertex needs to appear once per triangle.
		var vertices = new Float32Array( [
			-1.0, -1.0,  1.0,
			 1.0, -1.0,  1.0,
			 1.0,  1.0,  1.0,

			 1.0,  1.0,  1.0,
			-1.0,  1.0,  1.0,
			-1.0, -1.0,  1.0
		] );

		// itemSize = 3 because there are 3 values (components) per vertex
		geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
		target.mesh = new THREE.Mesh( geometry, target.shaderMaterial );
		target.scene.add(target.mesh);
		return target;
	}
	
	var WIDTH = window.innerWidth;
    var HEIGHT = window.innerHeight;

    var renderer = new THREE.WebGLRenderer({antialias:true});
	renderer.setSize(WIDTH, HEIGHT);
    renderer.setClearColor(0xDDDDDD, 1);
    document.body.appendChild(renderer.domElement);
	
	//TARGET1
	var render_target1 = make_render_target();
	
	//TARGET2
	var render_target2 = make_render_target();
	
	//visible target
	var render_target_visible = make_render_target();
	
	render_target1.shaderMaterial.fragmentShader = fragment_texture_pass_through_text;
	render_target2.shaderMaterial.fragmentShader = fragment_simulation_text;
	render_target_visible.shaderMaterial.fragmentShader = fragment_colors_text;

	//generate image on first render
	render_target1.shaderMaterial.uniforms.regenerate = {type: 'i', value:1};
	render_target1.shaderMaterial.uniforms.rects = {type: "v4v", value:null};
	render_target1.shaderMaterial.uniforms.temperatures = {type: "fv1", value:null};

	render_target1.shaderMaterial.uniforms.texture.value = render_target2.bufferTexture.texture;
	render_target2.shaderMaterial.uniforms.texture.value = render_target1.bufferTexture.texture;
	render_target_visible.shaderMaterial.uniforms.texture.value = render_target2.bufferTexture.texture;
	
	function render() {
		if(render_target1.shaderMaterial.uniforms.regenerate.value){
			console.log("generating image");
			generate_temperature_rects(render_target1.shaderMaterial.uniforms);
			console.log(render_target1.shaderMaterial.uniforms);
			renderer.render(render_target1.scene, render_target1.camera, render_target1.bufferTexture);
			render_target1.shaderMaterial.uniforms.regenerate.value = 0;
		}
		console.log("update");
		
		// target2 is rendered (from target1 texture) - at this point simulation happens
		renderer.render(render_target2.scene, render_target2.camera, render_target2.bufferTexture);
		// target1 is rendered (from target2 texture, passthrough shader is used)
		renderer.render(render_target1.scene, render_target1.camera, render_target1.bufferTexture);
		// target_visible is rendered (from target2 texture, temperatures are mapped to colors here)
		renderer.render( render_target_visible.scene, render_target_visible.camera );
	}

	window.setInterval(
		() => {requestAnimationFrame(render);},
		 1000/30
	);
	window.setInterval(
		() => {render_target1.shaderMaterial.uniforms.regenerate.value = 1;},
		 1000*30
	);
}