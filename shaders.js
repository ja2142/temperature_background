//TODO makes actual simulation 
const fragment_simulation_text =
`
#define HEAT_FLOW_RATE 0.02
uniform vec2 TEXTURE_SIZE;
uniform sampler2D texture;

float vec4unpack(vec4 v){
	return v.r + v.g/256. + v.b/(256.*256.);
}

vec4 vec4pack(float f){
	vec4 ret = vec4(floor(f*256.)/256., 0., 0., 0.);
	f -= ret.r;
	f *= 256.;
	
	ret.g = floor(f*256.)/256.;
	f -= ret.g*256.;
	f *= 256.;

	ret.b = floor(f*256.)/256.;

	return ret;
}

vec4 get_neighbour_pixel(sampler2D texture, float dx, float dy){
	return texture2D(texture, vec2(gl_FragCoord.x+dx, gl_FragCoord.y+dy)/TEXTURE_SIZE);
}

void main() {
	float old_temp = vec4unpack(texture2D(texture, gl_FragCoord.xy/TEXTURE_SIZE));
	float new_temp = old_temp;
	new_temp += 
		HEAT_FLOW_RATE*
		(vec4unpack(get_neighbour_pixel(texture, 1.0, 0.0)) - old_temp);
	new_temp += 
		HEAT_FLOW_RATE*
		(vec4unpack(get_neighbour_pixel(texture, -1.0, 0.0)) - old_temp);
	new_temp += 
		HEAT_FLOW_RATE*
		(vec4unpack(get_neighbour_pixel(texture, 0.0, 1.0)) - old_temp);
	new_temp += 
		HEAT_FLOW_RATE*
		(vec4unpack(get_neighbour_pixel(texture, 0.0, -1.0)) - old_temp);

	gl_FragColor = vec4pack(new_temp);
}
`

//passes through given texture on target
const fragment_texture_pass_through_text =
`
#define RECTS_SIZE 10
uniform vec2 TEXTURE_SIZE;
uniform sampler2D texture;
uniform int regenerate;
uniform float temperatures[RECTS_SIZE];
uniform vec4 rects[RECTS_SIZE];

bool fits_in_rect(vec4 rect, vec2 pos){
	return pos.x>rect.x&&pos.x<rect.z &&
		pos.y>rect.y&&pos.y<rect.w;
}

float point_temperature(vec2 pos){
	for(int i=0; i<RECTS_SIZE; i++){
		if(fits_in_rect(rects[i], pos)){
			return temperatures[i];
		}
	}
	return pos.x;
}

void main() {
	if(regenerate==1){
		gl_FragColor = vec4(point_temperature(gl_FragCoord.xy/TEXTURE_SIZE), 0.0, 0.0, 1.0);
	}else{
		gl_FragColor = texture2D(texture, gl_FragCoord.xy/TEXTURE_SIZE);
	}
}
`

//TODO changes temperature to colors 
const fragment_colors_text =
`
float vec4unpack(vec4 v){
	return v.r + v.g/256. + v.b/(256.*256.);
}

vec3 hsv_to_rgb(vec3 hsv) {
	float r, g, b, f, p, q, t, h, s, v;
	int i;
	h=hsv.r;
	s=hsv.g;
	v=hsv.b;

    i = int(floor(h * 6.));
    f = h * 6. - float(i);
    p = v * (1. - s);
    q = v * (1. - f * s);
	t = v * (1. - (1. - f) * s);
    
    if(i==0) r = v, g = t, b = p;
    if(i==1) r = q, g = v, b = p;
    if(i==2) r = p, g = v, b = t;
    if(i==3) r = p, g = q, b = v;
    if(i==4) r = t, g = p, b = v;
    if(i==5) r = v, g = p, b = q;
    
    return vec3(r,g,b);
}

//returns hsv vector from tempreture float [0,1]
vec3 temp_to_hsv(float temp){
	return vec3((1.-temp)*0.7, 0.9, 0.9);
}

uniform vec2 TEXTURE_SIZE;
uniform sampler2D texture;
void main() {
	gl_FragColor.rgb = hsv_to_rgb(temp_to_hsv(vec4unpack(texture2D(texture, gl_FragCoord.xy/TEXTURE_SIZE))));
	gl_FragColor.a = 1.;
}
`


const vertex_text =
`
void main() {
	gl_Position = projectionMatrix * modelViewMatrix * vec4(position.x, position.y, position.z, 1.0);
}
`